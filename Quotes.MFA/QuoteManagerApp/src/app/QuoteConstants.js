"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuoteConstants = void 0;
var QuoteConstants = /** @class */ (function () {
    function QuoteConstants() {
    }
    QuoteConstants.QuoteSource_Imonline = "1000003";
    QuoteConstants.QuoteSource_QM = "1000004";
    QuoteConstants.QuoteSource_IM360 = "2000000";
    QuoteConstants.QuoteStatus_Active = "Ready to Order";
    QuoteConstants.QuoteStatus_Draft = "Draft";
    return QuoteConstants;
}());
exports.QuoteConstants = QuoteConstants;
//# sourceMappingURL=QuoteConstants.js.map