import { Injectable } from '@angular/core';
import { SortDirection } from '@angular/material/sort';

@Injectable({
  providedIn: 'root'
})
export class QuoteListService {
  pageIndex:number;
  recordPerPage:number;
  sortColumn:string;
  sortDirection:SortDirection;

    constructor() {
      this.pageIndex=0;
      this.recordPerPage=25;
      this.sortColumn="createdon";
      this.sortDirection="desc";

     }
}
