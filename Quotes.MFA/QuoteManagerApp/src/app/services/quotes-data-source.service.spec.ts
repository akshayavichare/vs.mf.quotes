import { TestBed } from '@angular/core/testing';

import { QuotesDataSourceService } from './quotes-data-source.service';

describe('QuotesDataSourceService', () => {
  let service: QuotesDataSourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuotesDataSourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
