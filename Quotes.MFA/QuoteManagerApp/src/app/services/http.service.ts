import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {ConfigService} from '../services/config.service'
import { Observable } from 'rxjs';
import { QuoteListRequest,QuoteListResponse } from '../model';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private httpClient: HttpClient, private configService: ConfigService) { }

  getQuotes(request:QuoteListRequest):Observable<QuoteListResponse>{

    const params=new HttpParams()
.set('customernumber',request.customernumber)
.set('isocountrycode',request.isoCountryCode)
.set('pageIndex',request.pageIndex.toString())
.set('recordPerPage',request.recordPerPage.toString())
.set('Sorting',request.sortDirection)
.set('SortingColumnName',request.sortColumn)

const url = `http://localhost:14306/api/QuoteList/V1/GetQuotes`;
let headers = new HttpHeaders()
    headers=headers.append('Access-Control-Allow-Origin', '*');  
    headers=headers.append('Content-Type', 'application/json');  
    const options = { params: params, headers: headers };
    console.log(url);
    return this.httpClient.get<QuoteListResponse>(url, options);

  }
}
