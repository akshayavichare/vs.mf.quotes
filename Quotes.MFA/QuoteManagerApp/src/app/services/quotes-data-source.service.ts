import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { of } from 'rxjs/internal/observable/of';
import { pipe } from 'rxjs/internal/util/pipe';

import {QuoteListRequest,QuoteList} from '../../app/Model';
import { HttpService} from '../services/http.service';

@Injectable({
  providedIn: 'root'
})
export class QuotesDataSourceService implements DataSource<QuoteList> {

  private quotesSubject = new BehaviorSubject<QuoteList[]>([]);
  private loadingQuotes = new BehaviorSubject<boolean>(false);
  Quotecount:number;
  data : QuoteList[];

  public loading$ = this.quotesSubject.asObservable();

  constructor(private httpService: HttpService) { }
  connect(collectionViewer: CollectionViewer): Observable<QuoteList[] | readonly QuoteList[]> {
    return this.quotesSubject.asObservable();
  }
  disconnect(collectionViewer: CollectionViewer): void {
    this.quotesSubject.complete();
    this.loadingQuotes.complete();
  }
  LoadQuotes(quoteRequest: QuoteListRequest){

    this.loadingQuotes.next(true);
    this.httpService.getQuotes(quoteRequest)

    .subscribe(response => {
     this.quotesSubject.next(response.quotes);
      this.Quotecount = response.totalCount ? response.totalCount : this.Quotecount;
      this.data = response.quotes;

  },
  
  ()=>{this.loadingQuotes.next(false)}
  
  );


  }
}
