export class QuoteListRequest{
    pageIndex:number;
    recordPerPage:number;
    customernumber:string;
    isoCountryCode:string;
    sortColumn:string;
    sortDirection:string;
}

export class QuoteList{
    QuoteGuid:string
     QuoteName:string;
     QuoteNumber:string;
     CreatedBy:string;
     CreatedOn:Date;
     EndUserName:string;
     ModifiedOn:Date;
     Status:string;
     QuoteExpiryDate:Date;
     QuotePrice:string;
     RevisionNumber:string;
     QuoteNumber_REV: string;
     QuoteSource: string;
     QuoteType: string;    


}

export class QuoteListResponse{
    quotes:QuoteList[];
    totalCount:number;

}
