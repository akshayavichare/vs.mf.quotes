import { NgModule } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import {AppComponent} from './app.component'
import { QuotesComponent } from './pages/quotes/quotes.component';

const routes: Routes = [
  { path: '', redirectTo: 'quotes', pathMatch: 'full' },
  { path: 'quotes', component: QuotesComponent },
  { path: '**', component: EmptyRouteComponent }
];
export function getBaseUrl() {
  return '/';
}
@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes, { useHash: true }),CommonModule],
  exports: [RouterModule],
  providers:[
    { provide: APP_BASE_HREF, useFactory: getBaseUrl }
  ]
})
export class AppRoutingModule { }
