export class QuoteConstants {
  public static QuoteSource_Imonline = "1000003";
  public static QuoteSource_QM = "1000004";
  public static QuoteSource_IM360 = "2000000";
  public static QuoteStatus_Active = "Ready to Order";
  public static QuoteStatus_Draft = "Draft";

}
