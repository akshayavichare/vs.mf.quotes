import { HttpClientModule, HTTP_INTERCEPTORS,HttpClient } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import {MatSortModule} from '@angular/material/sort';
import { AppComponent } from './app.component';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { QuotesComponent } from './pages/quotes/quotes.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {ConfigService} from './services/config.service';
import { LoaderComponent } from './components/shared/loader/loader.component';
import { LoaderService } from './services/loader.service';
import { LoaderInterceptor } from './interceptors/loader-interceptor';


export function configLoader(configService: ConfigService) {
  return () => configService.load();
}

@NgModule({
  declarations: [
    AppComponent,
    QuotesComponent,
    EmptyRouteComponent,
    LoaderComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule,
   MatTableModule,
   MatPaginatorModule,
   MatSortModule,
   MatProgressSpinnerModule,
   BrowserAnimationsModule
  
  ],
  providers: [{provide:APP_INITIALIZER,useFactory:configLoader,deps:[ConfigService],multi:true},
  { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor,deps:[LoaderService],multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
