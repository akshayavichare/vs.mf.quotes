import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import {QuoteListService} from '../../services/quote-list.service'
import {ConfigService} from '../../services/config.service'
import {QuoteList, QuoteListRequest} from '../../model';
import {QuotesDataSourceService} from '../../services/quotes-data-source.service'
import { HttpService} from '../../services/http.service';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { tap } from 'rxjs/operators';
import {merge} from 'rxjs';
import { Router } from '@angular/router';
import { QuoteConstants } from '../../QuoteConstants'


@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css']
})
export class QuotesComponent implements OnInit,AfterViewInit {

  pageIndex:number;
  recordPerPage:number;
  resellerID:string;
  siteCode:string;
  

  quoterequest: QuoteListRequest =new QuoteListRequest();

  dataSource: QuotesDataSourceService;
  
  displayedColumns=[
    'createdOn',
    'quoteNumber_rev',
    'quoteName',
    'createdBy',
    'endUserName',
    'quotePrice',
    'modified',
    'status',
    'quoteexpirydate'
    
   
  ];
  @ViewChild(MatPaginator)
  paginator: MatPaginator;

  @ViewChild(MatSort)
  sort: MatSort;

  matSortColumn: string;
  matSortDirection: SortDirection;

  pageEvent:PageEvent;

  constructor(private quoteListService: QuoteListService, private configService: ConfigService, private httpService: HttpService,private router: Router) {}
   

  ngOnInit(): void {
    this.pageIndex=this.quoteListService.pageIndex;
    this.recordPerPage=this.quoteListService.recordPerPage;
    this.resellerID=this.configService.resellerId;
    this.siteCode=this.configService.siteCode;
    this.matSortColumn=this.quoteListService.sortColumn;
    this.matSortDirection=this.quoteListService.sortDirection;
    this.dataSource=new QuotesDataSourceService(this.httpService);

  }


  ngAfterViewInit() {


    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
    .pipe(tap(() => this.loadQuoteList())
    ).subscribe();

    setTimeout(() => {

      this.paginator.pageIndex = this.quoteListService.pageIndex;
      this.paginator.pageSize = this.quoteListService.recordPerPage;
      this.sort.active=this.quoteListService.sortColumn;
      this.sort.direction=this.quoteListService.sortDirection;

      this.loadQuoteList();
    });

  }

  getSortColumnName(activeColumn):string{
    let sortColumn='';
    switch(activeColumn){
      case 'createdOn':
        sortColumn='createdon';
        break;
      case 'quoteName':
        sortColumn='name';
        break;
      case 'endUserName':
        sortColumn='im360_resendusername';
        break;
      case 'quotePrice':
        sortColumn='totalamount';
        break;  
      case 'modified':
        sortColumn='modifiedon';
        break; 
      case 'quoteexpirydate':
        sortColumn='effectiveto';
         break;     

    }
    return sortColumn;
  }
  
  loadQuoteList(){

    this.quoteListService.pageIndex = this.paginator.pageIndex;
    this.quoteListService.recordPerPage = this.paginator.pageSize;
    this.quoteListService.sortColumn =this.getSortColumnName(this.sort.active);
    this.quoteListService.sortDirection = this.sort.direction;

    this.quoterequest.pageIndex=this.paginator.pageIndex + 1;
    this.quoterequest.recordPerPage=this.paginator.pageSize;
    this.quoterequest.sortColumn=this.getSortColumnName(this.sort.active);
    this.quoterequest.sortDirection=this.sort.direction;

    this.quoterequest.customernumber=this.resellerID;
    this.quoterequest.isoCountryCode=this.siteCode;
    this.dataSource.LoadQuotes(this.quoterequest);

console.log(this.dataSource);

  }

  onPaginationChange(event:PageEvent){
    let pageNo=event.pageIndex;
    let pageSize=event.pageSize;

    this.paginator.pageIndex = pageNo;
    this.paginator.pageSize = pageSize;
    this.loadQuoteList();
  }

  isQuoteListEmpty() {
    if (this.dataSource.data == null)
      return true;
    else
      return false;
  }

  GoToQuoteDetails(quote) {
    alert(quote.quoteGuid);
    //this.router.navigateByUrl('../../QuoteManager/QuoteDetails?quoteId=' + quote.quoteNumber + '&quoteGuid=' + quote.quoteGuid +'&fromQuoteList=true');
   // window.location.href = '../QuoteManager/QuoteDetail?quoteId=' + quote.quoteNumber_rev + '&fromQuoteList=true';
   

    if (quote.quoteSource === QuoteConstants.QuoteSource_Imonline) {
      window.location.href = '../VendorPartnerPortal/QuoteDetails?quoteId=' + quote.quoteNumber_rev + '&fromQuoteList=true';
    }
    else if (quote.quoteSource === QuoteConstants.QuoteSource_QM) {
      if (quote.status == QuoteConstants.QuoteStatus_Draft) {
        window.location.href = '../QuoteManager/EditQuote?quoteId=' + quote.quoteNumber_rev + '&quoteGuid=' + quote.quoteGuid + '&fromQuoteList=true';
      }
      else {
        window.location.href = '../QuoteManager/QuoteDetail?quoteId=' + quote.quoteNumber_rev + '&quoteGuid=' + quote.quoteGuid + '&fromQuoteList=true';
      }
    }
    else {
      window.location.href = '../QuoteManager/QuoteDetail?quoteId=' + quote.quoteNumber_rev + '&quoteGuid=' + quote.quoteGuid + '&fromQuoteList=true';
    }


  }

}
