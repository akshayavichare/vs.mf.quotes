import { Component } from '@angular/core';
import {ConfigService} from '../app/services/config.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'QuoteManagerApp';
  baseUrl: string;


  constructor(private config: ConfigService){

  }

  ngOnInit() {
    this.baseUrl = this.config.baseUrl;
    
  }
  
}
